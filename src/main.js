// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import vueResource from 'vue-resource'
import Main from './components/main'
import User from './components/user'
import RepoLangQuery from './components/repolang_query'


Vue.use(vueResource)
Vue.use(VueRouter)
Vue.config.productionTip = false
Vue.component('user',User)
/* eslint-disable no-new */
const router = new VueRouter({
  mode:'history',
  routes:[
    {path:'/',component:Main},
    {path:'/repo_lang/:name/get',component:RepoLangQuery},
  ]
})
new Vue({
  router,
  template: `
  
<router-view></router-view>
  `,
}).$mount('#app')
